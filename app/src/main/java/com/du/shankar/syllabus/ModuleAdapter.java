package com.du.shankar.syllabus;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



import java.util.ArrayList;

/**
 * Created by Shankar on 07-08-2016.
 */
public class ModuleAdapter extends ArrayAdapter<Module> {
    public ModuleAdapter(Activity context, ArrayList<Module> modules)
    {
        super(context,0,modules);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View listItemView=convertView;
        if(listItemView==null){
            listItemView= LayoutInflater.from(getContext()).inflate(R.layout.list_item,parent,false);
        }
        Module currentModule=getItem(position);
        TextView moduleNumber=(TextView)listItemView.findViewById(R.id.moduleNum);
        moduleNumber.setText(currentModule.getmModNum());
        TextView moduleName=(TextView)listItemView.findViewById(R.id.modName);
        String s=currentModule.getmModName();
        if(s.equals(""))
        {
            moduleName.setVisibility(View.GONE);
        }
        else{
            moduleName.setVisibility(View.VISIBLE);
            moduleName.setText(s);
        }
        TextView moduleDetails=(TextView)listItemView.findViewById(R.id.modDetail);
        String s2 = currentModule.getmModDetails();
        if(s2.equals("blah"))
        {
            moduleDetails.setVisibility(View.GONE);
        }
        else
        {
            moduleDetails.setVisibility(View.VISIBLE);
            moduleDetails.setText(s2);
        }

        return listItemView;

    }
}
