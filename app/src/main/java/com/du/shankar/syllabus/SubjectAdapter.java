package com.du.shankar.syllabus;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



import java.util.ArrayList;

/**
 * Created by Shankar on 21-08-2016.
 */
public class SubjectAdapter extends ArrayAdapter<Subject>{
    public SubjectAdapter(Activity context, ArrayList<Subject> modules)
    {

        super(context,0,modules);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View listItemView=convertView;
        if(listItemView==null){
            listItemView= LayoutInflater.from(getContext()).inflate(R.layout.list_item_subject,parent,false);
        }
        Subject currentSubject=getItem(position);
        TextView subjectName=(TextView)listItemView.findViewById(R.id.textViewSubject);
        subjectName.setText(currentSubject.getmSubName());

        return listItemView;

    }
}
