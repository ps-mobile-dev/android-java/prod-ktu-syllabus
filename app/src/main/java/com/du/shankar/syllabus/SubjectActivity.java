package com.du.shankar.syllabus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;



import java.util.ArrayList;

public class SubjectActivity extends AppCompatActivity{

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String sub[];
    String toShare;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);

        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);


        pref=getApplication().getSharedPreferences("Options", MODE_PRIVATE);
        editor=pref.edit();
        String title;
        final String sb=pref.getString("Sub","");
        editor.remove("Sub");
        editor.commit();
        sub=sb.split("/blah/");

        title=sub[1];
        setTitle(title);
        ArrayList<Module> modules=new ArrayList<Module>();
        int l=sub.length;

        modules.add(new Module("TEXT BOOKS",sub[2],"blah"));
        modules.add(new Module("About Subject","Subject Credits : "+sub[3]+"\n\nPrerequisits : "+sub[4],"blah"));
        for(int i=5;i<l-1;i+=2)
        {
            modules.add(new Module("Module "+((i/2)-1),sub[i],sub[i+1]));
        }

        ModuleAdapter adapter=new ModuleAdapter(this,modules);
        ListView listView=(ListView) findViewById(R.id.listModules);
        listView.setAdapter(adapter);


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View v,
                                           int index, long arg3) {
                if (index > 1) {

                    final AlertDialog.Builder build = new AlertDialog.Builder(SubjectActivity.this);
                    toShare = "Subject : " + sub[0] + "\n\n" + "Module " + (index) + " : " + sub[(index + 4)] + "\n\n" + sub[(index + 5)];
                    build.setTitle("Share via Whatsapp");
                    build.setMessage("Would you like to share the syllabus of " + sub[0] + " via WhatsApp?\n\nClick ALL to share the syllabus of the entire subject.\n\nClick MODULE to share the syllabus of selected module.\n\nClick CANCEL to dismiss.");
                    build.setPositiveButton("MODULE", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();

                            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                            whatsappIntent.setType("text/plain");
                            whatsappIntent.setPackage("com.whatsapp");
                            whatsappIntent.putExtra(Intent.EXTRA_TEXT, toShare);
                            try {
                                startActivity(whatsappIntent);
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast toast = Toast.makeText(SubjectActivity.this, "WhatsApp is not installed on your device.", Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        }
                    });
                    build.setNegativeButton("ALL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            toShare = "Subject : " + sub[0];
                            for (int i = 5; i < sub.length; i += 2) {
                                toShare = toShare + "\n\n" + "Module : " + ((i /2)-1) + "\n\n" + sub[i] + "\n\n" + sub[i + 1];
                            }
                            toShare=toShare+ "\n\n" + "Text Books :\n\n" + sub[2]+"\n\nSubject Credits : "+sub[3]+"\n\nSubject Prerequisits : "+sub[4];
                            dialog.dismiss();
                            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                            whatsappIntent.setType("text/plain");
                            whatsappIntent.setPackage("com.whatsapp");
                            whatsappIntent.putExtra(Intent.EXTRA_TEXT, toShare);
                            try {
                                startActivity(whatsappIntent);
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast toast = Toast.makeText(SubjectActivity.this, "WhatsApp is not installed on your device.", Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        }
                    });
                    build.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    build.show();
                } else if (index == 0)
                {
                    toShare = "Subject : " + sub[0] + "\n\n" + "Text Books :\n\n" + sub[2];
                    final AlertDialog.Builder build = new AlertDialog.Builder(SubjectActivity.this);
                    build.setTitle("Share via Whatsapp");
                    build.setMessage("Would you like to share the reference text books of  " + sub[0] + " via WhatsApp?");
                    build.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                            whatsappIntent.setType("text/plain");
                            whatsappIntent.setPackage("com.whatsapp");
                            whatsappIntent.putExtra(Intent.EXTRA_TEXT, toShare);
                            try {
                                startActivity(whatsappIntent);
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast toast = Toast.makeText(SubjectActivity.this, "WhatsApp is not installed on your device.", Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        }
                    });
                    build.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();

                        }
                    });
                    build.show();
                }
                else if (index == 1)
                {
                    toShare = "Subject : " + sub[0] +"\n\nSubject Credits : "+sub[3]+"\n\nSubject Prerequisits : "+sub[4];
                    final AlertDialog.Builder build = new AlertDialog.Builder(SubjectActivity.this);
                    build.setTitle("Share via Whatsapp");
                    build.setMessage("Would you like to share the credits and prerequisits of  " + sub[0] + " via WhatsApp?");
                    build.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                            whatsappIntent.setType("text/plain");
                            whatsappIntent.setPackage("com.whatsapp");
                            whatsappIntent.putExtra(Intent.EXTRA_TEXT, toShare);
                            try {
                                startActivity(whatsappIntent);
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast toast = Toast.makeText(SubjectActivity.this, "WhatsApp is not installed on your device.", Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        }
                    });
                    build.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();

                        }
                    });
                    build.show();
                }
                    return true;
                }

        });


    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent i=new Intent(SubjectActivity.this,SemesterActivity.class);
        editor.remove("Sub");
        editor.commit();
        startActivity(i);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent i=new Intent(SubjectActivity.this,SemesterActivity.class);
                editor.remove("Sub");
                editor.commit();
                startActivity(i);
                finish();
                return(true);
        }

        return(super.onOptionsItemSelected(item));
    }
}
