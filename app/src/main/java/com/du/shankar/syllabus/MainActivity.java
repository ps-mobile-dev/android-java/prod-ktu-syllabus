package com.du.shankar.syllabus;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    int mBranch;
    SharedPreferences.Editor editor;
    int sem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.branches_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        TextView s1 = (TextView) findViewById(R.id.s1textView);
        TextView s3 = (TextView) findViewById(R.id.s3textView3);
        TextView s4 = (TextView) findViewById(R.id.s4textView4);
        TextView s5 = (TextView) findViewById(R.id.s5textView5);
        TextView s6 = (TextView) findViewById(R.id.s6textView6);
        TextView s7 = (TextView) findViewById(R.id.s7textView7);
        TextView s8 = (TextView) findViewById(R.id.s8textView8);
        spinner.setOnItemSelectedListener(this);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("Options", MODE_PRIVATE);
        mBranch = pref.getInt("Batch", 0);
        spinner.setSelection(mBranch);
        editor = pref.edit();
        editor.clear();

        s1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(MainActivity.this, SemesterActivity.class);
                sem = 1;
                edit();
                startActivity(n);
            }
        });


        s3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(MainActivity.this, SemesterActivity.class);
                sem = 3;
                edit();
                startActivity(n);
            }
        });

        s4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(MainActivity.this, SemesterActivity.class);
                sem = 4;
                edit();
                startActivity(n);
            }
        });
        s5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(MainActivity.this, SemesterActivity.class);
                sem = 5;
                edit();
                startActivity(n);
            }
        });
        s6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(MainActivity.this, SemesterActivity.class);
                sem = 6;
                edit();
                startActivity(n);
            }
        });
        s7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(MainActivity.this, SemesterActivity.class);
                sem = 7;
                edit();
                startActivity(n);
            }
        });
        s8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(MainActivity.this, SemesterActivity.class);
                sem = 8;
                edit();
                startActivity(n);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.button, menu);

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.action_about:
                about();
                return true;
            //case R.id.action_credits:
            //    credit();
             //   return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemSelected(AdapterView parent, View view, int position, long id) {
        // On selecting a spinner item

        // String item = parent.getItemAtPosition(position).toString();
        // To get selection in string format
        mBranch = position;

        // Showing selected spinner item


    }



    public void onNothingSelected(AdapterView parent) {
        // TODO Auto-generated method stub

    }


    public void edit() {
        editor.putInt("Sem", sem);
        editor.putInt("Batch", mBranch);
        editor.commit();
    }
    public void about(){

        final Dialog dialog_about=new Dialog(this);
        dialog_about.setContentView(R.layout.custom_about_dialog);
        dialog_about.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_about.show();
    }
    /*public void credit(){

        final AlertDialog.Builder build = new AlertDialog.Builder(MainActivity.this);
        build.setTitle("CREDITS");
        build.setMessage(R.string.credits);
        build.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();}
        });
        build.show();
    }
    */
}
